<?php

namespace cubegroup\modules\UserManagement\controllers;

use Yii;
use cubegroup\modules\UserManagement\models\UserVisitLog;
use cubegroup\modules\UserManagement\models\search\UserVisitLogSearch;
use cubegroup\components\AdminDefaultController;

/**
 * UserVisitLogController implements the CRUD actions for UserVisitLog model.
 */
class UserVisitLogController extends AdminDefaultController
{
	/**
	 * @var UserVisitLog
	 */
	public $modelClass = 'cubegroup\modules\UserManagement\models\UserVisitLog';

	/**
	 * @var UserVisitLogSearch
	 */
	public $modelSearchClass = 'cubegroup\modules\UserManagement\models\search\UserVisitLogSearch';

	public $enableOnlyActions = ['index', 'view', 'grid-page-size'];
}
